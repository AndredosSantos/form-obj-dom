const form = document.getElementById("form");

const divUsers = document.getElementById("users");

const usersList = [];

form.addEventListener("submit", (e) => {
  e.preventDefault();

  const childrens = form.children;
  let user = {};

  for (let indice = 0; indice < childrens.length; indice++) {
    const nodeName = childrens[indice].nodeName;

    if (nodeName === "INPUT") {
      const children = childrens[indice];

      if (children.name == "name") {
        const name = children.value;
        user["name"] = name;
      }
      if (children.name == "email") {
        const email = children.value;
        user["email"] = email;
      }
      if (children.name == "age") {
        const age = children.value;
        user["age"] = age;
      }
    }
  }

  usersList.push(user);

  showUsers();
});

const showUsers = () => {
  divUsers.innerHTML = "";
  const ul = document.createElement("ul");

  for (let indice = 0; indice < usersList.length; indice++) {
    const user = usersList[indice];

    const li = document.createElement("li");
    const h1 = document.createElement("h1");
    const h2 = document.createElement("h2");
    const h3 = document.createElement("h3");

    h1.innerText = `Nome: ${user.name}`;
    h2.innerText = `Email: ${user.email}`;
    h3.innerText = `Age: ${user.age}`;

    li.appendChild(h1);
    li.appendChild(h2);
    li.appendChild(h3);

    ul.appendChild(li);
  }

  divUsers.appendChild(ul);
};
